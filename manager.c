#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "err.h"

#ifndef DEBUG
int debug = 0;
#else
int debug = 1;
#endif

/** Liczba parametrów z którymi jest wywoływany
 *  manager.
 */
int const PARAMS_NO = 3;

/** Ścieżka do katalogu zawierającego pliki
 *  z danymi.
 */
char* DIRECTORY = "DATA/";

/** Czyta z pliku input linię line_no zawierającą
 *  wyrażenie i przekazuje je wraz z numerem
 *  linii do pierwszego podwykonawcy.
 */
void process_new(FILE *input, int line_no)
{
    char tmp = 0;
    char message[10];
    if (debug) fprintf(stderr, "Przetwarzanie linii %d\n", line_no);
    /* przekazuje numer linii */
    printf("%d ", line_no);
    /* przekazuje wyrażenie */
    while (tmp != '\n') {
        fscanf(input, "%s%c", message, &tmp);
        printf("%s%c", message, tmp);
    }
    fflush(stdout);
}

/** Odbiera informację od podwykonawcy
 *  i w zależności od zawartości,
 *  zapisuje wynik do pliku output lub przekazuje
 *  wyrażenie do dalszych obliczeń.
 */
void process_old(FILE* output, int *under_calculation, int *lines_left)
{
    int line_no;
    char tmp;
    char message[10];
    /* czyta numer linii */
    scanf("%d%c", &line_no, &tmp);
    /* czyta pierwszy składnik wyrażenia, aby ustalić 
     * czy jest to wynik końcowy (tmp == '\n') 
     * czy część nieobliczonego do końca wyrażenia (tmp != '\n') */
    scanf("%s%c", message, &tmp);
    if (tmp == '\n') { /* koniec przetwarzania */
        if (debug) fprintf(stderr, "Koniec przetwarzania linii"
                        " %d. Wynik: %s\n", line_no, message);
        fprintf(output, "%d: %s\n", line_no, message);
        fflush(output);
        --(*lines_left);
        --(*under_calculation);
        if (debug) fprintf(stderr, "Pozostało %d linii do"
                    " przetowrzenia\n", *lines_left);
        return;
    }
    /* przekazuje numer linii i pierwszy składnik dalej */
    if (debug) fprintf(stderr, "Przekazuję linię %d dalej\n", line_no);
    printf("%d %s ", line_no, message);
    
    /* przekazuje pozostałe składniki podwykonawcy */
    while (tmp != '\n') {
        scanf("%s%c", message, &tmp);
        printf("%s%c", message, tmp);
    }
    fflush(stdout);
}

int main(int argc, char* argv[])
{
    int N;
    char input_path[50], output_path[50];
    int pipe_out_desc[2], pipe_in_desc[2];
    FILE* input, * output;
    int lines_no, lines_left, lines_read, under_calculation;

    if (argc < PARAMS_NO) {
        printf("Error: too few parameters. Provide number of executors,"
               "data file name, and output file name.\n");
        exit(EXIT_FAILURE);
    }

    N = atoi(argv[1]);    
    strcpy(input_path, DIRECTORY);
    strcat(input_path, argv[2]);
    strcpy(output_path, DIRECTORY);
    strcat(output_path, argv[3]);
    
    if (debug) fprintf(stderr, "Wczytano N: %d, input_path: %s, "
                       "output_path: %s\n", N, input_path, output_path);
    
    if ((input = fopen(input_path, "r")) == NULL)
        syserr("Error in fopen(input_path, \"r\")\n");
    if ((output = fopen(output_path, "w")) == NULL)
        syserr("Error in fopen(output_path, \"w\")\n");

    if (pipe(pipe_out_desc) == -1)
        syserr("Error in pipe\n");
    if (pipe(pipe_in_desc) == -1)
        syserr("Error in pipe\n");
    
    switch (fork()) {
    case -1:
        syserr("Error in fork\n");

    case 0: /*dziecko*/
        if (close(0) == -1) 
            syserr("[child] Error in close(0)\n");
        if (close(1) == -1) 
            syserr("[child] Error in close(1)\n");
        if (close(pipe_out_desc[1]))
            syserr("[child] Error in close(pipe_out_desc[1])");
        if (close(pipe_in_desc[0]))
            syserr("[child] Error in close(pipe_out_desc[0])");

        if (dup(pipe_out_desc[0]) == -1)
            syserr("[child] Error in dup(pipe_out_desc[0])\n");
        if (dup(pipe_in_desc[1]) == -1)
            syserr("[child] Error in dup(pipe_in_desc[1])\n");
        if (close(pipe_out_desc[0]) == -1) 
            syserr("[child] Error in close(pipe_out_desc[0])\n");
        if (close(pipe_in_desc[1]) == -1) 
            syserr("[child] Error in close(pipe_in_desc[1])\n");

        if (debug) fprintf(stderr, "manager odpala execa\n");
        execl("./executor", "executor", argv[1], NULL);
        syserr("Error in execl\n");

    default: /*rodzic*/
        if (close(0) == -1) 
            syserr("Error in close(0)\n");
        if (close(1) == -1) 
            syserr("Error in close(1)\n");
        if (close(pipe_out_desc[0]))
            syserr("Error in close(pipe_out_desc[0])");
        if (close(pipe_in_desc[1]))
            syserr("Error in close(pipe_out_desc[1])");

        if (dup(pipe_in_desc[0]) == -1)
            syserr("Error in dup(pipe_in_desc[0])\n");
        if (dup(pipe_out_desc[1]) == -1)
            syserr("Error in dup(pipe_out_desc[1])\n");
        if (close(pipe_in_desc[0]) == -1) 
            syserr("Error in close(pipe_in_desc[0])\n");
        if (close(pipe_out_desc[1]) == -1) 
            syserr("Error in close(pipe_out_desc[1])\n");

        fscanf(input, "%d", &lines_no);
        if (debug) fprintf(stderr, "Linii do przetworzenia: %d\n", 
                            lines_no);
        /* linie wczytane z pliku wejściowego */
        lines_read = 0;
        /* linie pozostałe do przetworzenia/zapisania do pliku 
         * wyjściowego */
        lines_left = lines_no;
        /* linie w trakcie przetwarzania */
        under_calculation = 0;
        while (lines_left > 0) {
            if (lines_read < lines_no && under_calculation < N) {
                if (debug) fprintf(stderr, "Czytam wyrażenie z pliku\n");
                ++lines_read;
                ++under_calculation;
                process_new(input, lines_read);
            } else {
                if (debug) fprintf(stderr, "Czytam wyrażenie od"
                                    " podwykonawcy\n");
                process_old(output, &under_calculation, &lines_left);
            }
            if (debug) fprintf(stderr, "Linii wczytanych: %d\n"
                            "Linii do przetworzenia: %d\n"
                            "Linii przetwarzanych: %d\n", lines_read,
                            lines_left, under_calculation);
        }
        printf("0\n");
        fflush(stdout);
        
        if (wait (0) == -1)
            syserr("Error in wait\n");
        
        if (fclose(input) == -1) 
            syserr("Error in fclose(input)\n");
        if (fclose(output) == -1) 
            syserr("Error in fclose(output)\n");
        
        exit(EXIT_SUCCESS);
    } /* switch */
}
