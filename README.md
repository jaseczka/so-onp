# Description #
## The Ring Calculator ##
### Operating Systems Homework #1 ###

Processes communicate with each other using one-direction unnamed pipes, forming a ring. Manager process creates and manages child processes. Each child process calculates one part of algebraic expression and passes the outcome to the next child. Only manager scans input and saves output.