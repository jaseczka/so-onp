#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include "err.h"

#ifndef DEBUG
int debug = 0;
#else
int debug = 1;
#endif

void process_line_no(int pid, int N) 
{
    int line_no;
    char tmp;

    scanf("%d%c", &line_no, &tmp);
    printf("%d%c", line_no, tmp);
    
    if (debug) fprintf(stderr, "[E%d] wczytano linię %d\n", N, line_no);

    if (line_no) 
        return;

    fflush(stdout);
    
    if (debug) fprintf(stderr, "[E%d] koniec działania\n", N);
    
    if (!pid) /*dziecko*/
        exit(EXIT_SUCCESS);
    
    if (wait(0) == -1)
        syserr("Error in wait");
    
    exit(EXIT_SUCCESS);    
}

int is_operator(char* test)
{
    if (strlen(test) > 1)
        return 0;
    
    switch (test[0]) {
    case '+':
    case '-':
    case '*':
    case '/': return 1;
    default:  return 0;
    }
}

int result(char message[][10])
{
    int a, b;
    a = atoi(message[0]);
    b = atoi(message[1]);
    if (debug) fprintf(stderr, "Obliczam wynik %d %s %d\n", 
                        a, message[2], b);
    switch (message[2][0]) {
    case '+': return a + b;
    case '-': return a - b;
    case '*': return a * b;
    case '/': return a / b;
    default : return 0;
    }
}

int main(int argc, char* argv[])
{
    int N = atoi(argv[1]);
    int pipe_desc[2];
    int pid = 0;
    char message[3][10];
    char tmp = 0;

    if (debug) fprintf(stderr, "Executorów do utworzenia: %s\n", 
                        argv[1]);
    --N;
    
    while (N && !pid) { /* jeśli N > 0 i jestem potomkiem */
        if (pipe(pipe_desc) == -1)
            syserr("Error in pipe\n");

        if (debug) fprintf(stderr, "Robimy forka\n");
        
        switch(pid = fork()) {
        case -1:
            syserr("Error in fork\n");

        case 0: /*dziecko*/
            if (close(pipe_desc[1]) == -1)
                syserr("Error in close(pipe_desc[1])");
            if (close(0) == -1)
                syserr("Error in close(0)");
            if (dup(pipe_desc[0]) == -1)
                syserr("Error in cup(pipe_desc[0]");
            if (close(pipe_desc[0]) == -1)
                syserr("Error in close(pipe_desc[0])");
            --N;
            break;

        default: /*rodzic*/
            if (close(pipe_desc[0]) == -1)
                syserr("Error in close(pipe_desc[0])");
            if (close(1) == -1)
                syserr("Error in close(1)");
            if (dup(pipe_desc[1]) == -1)
                syserr("Error in cup(pipe_desc[1]");
            if (close(pipe_desc[1]) == -1)
                syserr("Error in close(pipe_desc[1])");
        }
    }

    for ( ; ; ) {
        if (debug) fprintf(stderr, "[E%d] czytanie linii\n", N);
        process_line_no(pid, N);
        if (debug) fprintf(stderr, "[E%d] czytanie wyrażeń\n", N);
        scanf("%s%c", message[0], &tmp);
        if (debug) fprintf(stderr, "[E%d] wczytano %s%c.\n", 
                            N, message[0], tmp);
        if (tmp == '\n') { /* koniec linii = wynik */
            if (debug) fprintf(stderr, "[E%d] wynik końcowy."
                                " nie ma nic do roboty\n", N);
            printf("%s%c", message[0], tmp);
        } else { /* szukamy wyrażenia do obliczenia */
            if (debug) fprintf(stderr, "[E%d] będzie robota\n", N);
            scanf("%s", message[1]);
            scanf("%s%c", message[2], &tmp);
            if (debug) fprintf(stderr, "[E%d] wczytano: %s %s %s\n", 
                            N, message[0], message[1], message[2]);
            while (!is_operator(message[2])) {
                if (debug) fprintf(stderr, "[E%d] nie operator: %s",
                                    N, message[2]);
                if (debug) fprintf(stderr, "[E%d] przkazuje"
                                " dalej %s\n", N, message[0]);
                printf("%s ", message[0]);
                strcpy(message[0], message[1]);
                strcpy(message[1], message[2]);
                scanf("%s%c", message[2], &tmp);
                if (debug) fprintf(stderr, "[E%d] wczytano: %s %s %s\n", 
                                N, message[0], message[1], message[2]);
            } /* message[2] zawiera operator */
            /* drukuj wynik */
            if (debug) fprintf(stderr, "[E%d] przekazuję %d%c.\n",
                                N, result(message), tmp);
            printf("%d%c", result(message), tmp);
            if (debug) fprintf (stderr, "[E%d] przekazywanie reszty\n", 
                                 N);
            /* przepisz resztę wejścia jeśli taka jest */
            while (tmp != '\n') {
                scanf("%s%c", message[0], &tmp);
                if (debug) fprintf(stderr, "[E%d] przkazuje"
                                " dalej %s\n", N, message[0]);
                printf("%s%c", message[0], tmp);
            }
        }
        fflush(stdout);
    }

    if (debug) fprintf(stderr, "[E%d] wyrzuciło z pętli\n", N);
    exit(EXIT_FAILURE);
}
